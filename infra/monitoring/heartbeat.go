package monitoring

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func HeartbeatHandler(router *gin.Engine) {
	router.GET("/heartbeat", func(c *gin.Context) {
		c.Status(http.StatusOK)
	})
}