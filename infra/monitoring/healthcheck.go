package monitoring

import (
	"github.com/gin-gonic/gin"
	"github.com/go-pg/pg/v10"
	"github.com/tidwall/gjson"
	"lurgat/infra/storage"
	"lurgat/packages/bookmark"
	"net/http"
)

func HealthcheckHandler(router *gin.Engine, db *pg.DB, elastic *storage.Elastic) {
	router.GET("/healthcheck", func(c *gin.Context) {
		var details string
		status := "UP"

		count, err := db.Model(&bookmark.Bookmark{}).Count()
		resBytes, err := elastic.Count("bookmarks")
		esCount := gjson.GetBytes(resBytes.Bytes(), "count").Int()

		if err != nil {
			status = "ERROR"
		}

		sync := int64(count) == esCount
		if !sync {
			status = "WARNING"
			details = "Data must be re-synchronised"
		}

		c.JSON(http.StatusOK, gin.H{
			"status": status,
			"details": details,
			"sync":   sync,
			"items": count,
		})
	})
}