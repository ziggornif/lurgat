package storage

import (
	"errors"

	"lurgat/infra"

	"github.com/go-pg/pg/v10"
	"github.com/sirupsen/logrus"
)

var db *pg.DB

func DBConnect(config *infra.Configuration, logger *logrus.Logger) (*pg.DB, error) {
	if db != nil {
		return db, nil
	}

	opts := &pg.Options{
		Addr:     config.DBURL,
		User:     config.DBUser,
		Password: config.DBPass,
		Database: config.DBName,
	}

	db = pg.Connect(opts)
	if db == nil {
		logger.Error("Fail to connect to database.")
		return nil, errors.New("fail to connect to database")
	}

	logger.Info("Connection to database successful.")
	return db, nil
}

func DBClose(logger *logrus.Logger) {
	_ = db.Close()
	db = nil
}
