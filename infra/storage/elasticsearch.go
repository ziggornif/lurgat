package storage

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"lurgat/infra"
	"strings"

	"github.com/elastic/go-elasticsearch/v7"
	"github.com/sirupsen/logrus"
)

type Elastic struct {
	Client *elasticsearch.Client
	logger *logrus.Logger
}

var es *Elastic

func ESConnect(config *infra.Configuration, logger *logrus.Logger) (*Elastic, error) {
	cfg := elasticsearch.Config{
		Addresses: []string{config.ESURL},
	}
	var err error
	if es != nil {
		return es, nil
	}

	esClient, err := elasticsearch.NewClient(cfg)
	if err != nil {
		logger.Error("Fail to connect to elasticsearch.")
		return nil, errors.New("fail to connect to elasticsearch")
	}

	logger.Info("Connection to elasticsearch successful.")
	es := &Elastic{
		Client: esClient,
		logger: logger,
	}

	return es, nil
}

func ESClose() {
	es = nil
}

func (e *Elastic) CreateIndex(name string, mapping string) error {
	res, err := e.Client.Indices.Exists([]string{name})
	if err != nil {
		e.logger.Errorf("[Elasticsearch] CreateIndex - Error while requesting elasticsearch, Reason: %s", err)
		return err
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			e.logger.Errorf("Error closing query: %s", err)
		}
	}(res.Body)


	if res.StatusCode == 404 {
		res, err := e.Client.Indices.Create(
			name,
			e.Client.Indices.Create.WithBody(strings.NewReader(mapping)),
			e.Client.Indices.Create.WithWaitForActiveShards("1"),
		)
		e.logger.Infof("[Elasticsearch] CreateIndex - Index %s created", name)
		e.logger.Debugf("... context : %s", mapping)
		if err != nil || res.IsError() {
			e.logger.Errorf("[Elasticsearch] CreateIndex - Error while creating index, Reason: %s", err)
			return err
		}
		if res.IsError() {
			e.logger.Errorf("[Elasticsearch] CreateIndex - Error while creating index, Reason: %s", err)
			return fmt.Errorf("error while creating index %s", res)
		}
	}

	e.logger.Info("[Elasticsearch] CreateIndex - Index already exists")
	return nil
}

func (e *Elastic) DeleteIndex(name string) {
	_, err := e.Client.Indices.Delete([]string{name})
	if err != nil {
		e.logger.Errorf("Error while deleting index : %s", err)
	}
}

func (e *Elastic) Search(index string, query string) (*bytes.Buffer, error) {
	var bytesRes bytes.Buffer
	res, err := e.Client.Search(
		e.Client.Search.WithContext(context.Background()),
		e.Client.Search.WithIndex(index),
		e.Client.Search.WithBody(strings.NewReader(query)),
		e.Client.Search.WithTrackTotalHits(true),
		e.Client.Search.WithPretty(),
	)
	if err != nil {
		e.logger.Errorf("Error getting response: %s", err)
		return nil, err
	}

	if res.StatusCode == 404 {
		return nil, errors.New("index not found")
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			e.logger.Errorf("Error closing query: %s", err)
		}
	}(res.Body)

	_, err = bytesRes.ReadFrom(res.Body)
	if err != nil {
		return nil, err
	}
	return &bytesRes, nil
}

func (e *Elastic) Count(index string) (*bytes.Buffer, error) {
	var bytesRes bytes.Buffer
	res, err := e.Client.Count(
		e.Client.Count.WithIndex("bookmarks"),
		e.Client.Count.WithPretty(),
	)
	if err != nil {
		e.logger.Errorf("Error getting response: %s", err)
		return nil, err
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			e.logger.Errorf("Error closing query: %s", err)
		}
	}(res.Body)

	_, err = bytesRes.ReadFrom(res.Body)
	if err != nil {
		return nil, err
	}
	return &bytesRes, nil
}
