CLIENT_DIR=client
CLIENT_DIST_DIR=static
COVERAGE_DIR=coverage
MAIN=main.go

run_deps: destroy_deps
	cd docker && docker-compose up -d
	sleep 15
	curl http://localhost:9200

destroy_deps:
	cd docker && docker-compose down -v

# Server Tasks

audit_server:
	gosec ./...

lint_server:
	revive -config defaults.toml -formatter friendly ./...

test_server:
	rm -rf $(COVERAGE_DIR) && mkdir $(COVERAGE_DIR)
	go test ./... -v 2>&1 | go-junit-report -set-exit-code > coverage/junit.xml

run_test_server: run_deps test_server destroy_deps

build_server:
	go build -ldflags "-w -s" -o lurgat $(MAIN)

# UI Tasks

install_ui:
	cd $(CLIENT_DIR) && npm ci

audit_ui:
	cd $(CLIENT_DIR) && npm audit

lint_ui:
	cd $(CLIENT_DIR) && npm run lint

test_ui:
	cd $(CLIENT_DIR) && npm run test:unit

build_ui:
	rm -rf ./static
	cd $(CLIENT_DIR) && npm ci && npm run build
	mv $(CLIENT_DIR)/dist ./$(CLIENT_DIST_DIR)

# Global Tasks

lint: lint_ui lint_server

test: test_ui test_server

build: build_ui build_server

unused:
	go mod tidy
	
run_app:
	./lurgat

run: run_deps run_app
