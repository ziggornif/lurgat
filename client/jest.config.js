module.exports = {
  preset: "@vue/cli-plugin-unit-jest",
  testResultsProcessor: "jest-junit",
  testMatch: ["**/*.spec.{j,t}s?(x)"],
  transform: {
    "^.+\\.vue$": "vue-jest"
  },
  collectCoverage: true,
  collectCoverageFrom: [
    "src/**/*.{js,vue}",
    "!src/*.js",
    "!src/**/*.spec.js",
    "!**/node_modules/**"
  ]
};
