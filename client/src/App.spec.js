import axios from "axios";
import { shallowMount } from "@vue/test-utils";
import flushPromises from "flush-promises";
import Bookmarks from "@/components/Bookmarks.vue";
import Searchbar from "@/components/Searchbar.vue";
import FilterStore from "@/stores/filter.store";
import App from "./App.vue";

jest.mock("axios");

describe("App", () => {
  const bookmarks = [
    {
      id: "1",
      title: "bookmark",
      description: "Awesome description",
      link: "https://example.com",
      tags: ["awesome", "tags"],
      date: new Date().toISOString()
    },
    {
      id: "2",
      title: "click here",
      description: "wow !",
      link: "https://my-link.com",
      tags: ["shitty", "post"],
      date: new Date().toISOString()
    }
  ];

  beforeEach(() => {
    jest.clearAllMocks();
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it("should render bookmarks list", async () => {
    axios.mockImplementation(async () => {
      return {
        data: bookmarks
      };
    });

    const wrapper = shallowMount(App, {
      propsData: {
        store: FilterStore.state
      }
    });

    await flushPromises();

    expect(wrapper.html()).toContain("<sidebar-stub></sidebar-stub>");
    expect(wrapper.html()).toContain("<searchbar-stub minchars=\"3\"></searchbar-stub>");
    expect(wrapper.html()).toContain(
      "<bookmarks-stub bookmarks=\"[object Object],[object Object]\"></bookmarks-stub>"
    );

    expect(axios).toBeCalled();
  });

  it("should manage bookmarks tag filter event", async () => {
    const bookmarksMock = [
      {
        id: "1",
        title: "bookmark",
        description: "Awesome description",
        link: "https://example.com",
        tags: ["awesome", "tags"],
        date: new Date().toISOString()
      }
    ];

    FilterStore.setActiveTag("awesome");
    FilterStore.setActiveDate("daily");

    axios.mockImplementation(async () => {
      return {
        data: bookmarksMock
      };
    });

    const wrapper = shallowMount(App, {
      propsData: {
        store: FilterStore.state
      }
    });

    await flushPromises();

    wrapper.findComponent(Bookmarks).vm.$emit("tag-filtred");
    expect(FilterStore.state.searchQuery).toBeNull();
    expect(axios).toHaveBeenCalledWith({
      method: "GET",
      url: "/api/bookmarks/",
      params: {
        since: "daily",
        tags: "awesome"
      }
    });

    expect(axios).toBeCalled();
  });

  it("should manage search cancel event", async () => {
    const bookmarksMock = [
      {
        id: "1",
        title: "bookmark",
        description: "Awesome description",
        link: "https://example.com",
        tags: ["awesome", "tags"],
        date: new Date().toISOString()
      }
    ];

    axios.mockImplementation(async () => {
      return {
        data: bookmarksMock
      };
    });

    const wrapper = shallowMount(App, {
      propsData: {
        store: FilterStore.state
      }
    });

    await flushPromises();

    wrapper.findComponent(Searchbar).vm.$emit("search-cancel");
    expect(FilterStore.state.searchQuery).toBeNull();
    expect(axios).toBeCalledTimes(2);
    expect(axios).toHaveBeenCalledWith({
      method: "GET",
      url: "/api/bookmarks/"
    });

    expect(axios).toBeCalled();
  });

  it("should manage search event", async () => {
    const bookmarksMock = [
      {
        id: "1",
        title: "bookmark",
        description: "Awesome description",
        link: "https://example.com",
        tags: ["awesome", "tags"],
        date: new Date().toISOString()
      }
    ];

    axios.mockImplementation(async () => {
      return {
        data: bookmarksMock
      };
    });

    FilterStore.setActiveTag("awesome");
    FilterStore.setActiveDate("daily");
    FilterStore.setSearchQuery("mysearch");

    const wrapper = await shallowMount(App, {
      propsData: {
        store: FilterStore.state
      }
    });

    await flushPromises();

    wrapper.findComponent(Searchbar).vm.$emit("search");
    expect(FilterStore.state.activeDate).toBeNull();
    expect(FilterStore.state.activeTag).toBeNull();
    expect(axios).toBeCalledTimes(2);
    expect(axios).toHaveBeenCalledWith({
      method: "GET",
      url: "/api/bookmarks/?search=mysearch"
    });
    expect(axios).toBeCalled();
  });
});
