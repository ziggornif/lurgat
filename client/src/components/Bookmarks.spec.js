import { shallowMount } from "@vue/test-utils";
import Bookmarks from "@/components/Bookmarks.vue";
import Bookmark from "@/components/Bookmark.vue";

describe("Bookmarks", () => {
  const bookmarks = [
    {
      id: "1",
      title: "bookmark",
      description: "Awesome description",
      link: "https://example.com",
      tags: ["awesome", "tags"],
      date: new Date().toISOString()
    },
    {
      id: "2",
      title: "click here",
      description: "wow !",
      link: "https://my-link.com",
      tags: ["shitty", "post"],
      date: new Date().toISOString()
    }
  ];
  it("should render bookmarks list", () => {
    const wrapper = shallowMount(Bookmarks, {
      propsData: {
        bookmarks
      }
    });
    expect(wrapper.html()).toContain(
      '<bookmark-stub title="bookmark" description="Awesome description" link="https://example.com" tags="awesome,tags"></bookmark-stub>'
    );
    expect(wrapper.html()).toContain(
      '<bookmark-stub title="bookmark" description="Awesome description" link="https://example.com" tags="awesome,tags"></bookmark-stub>'
    );
  });

  it("should transfer filter event", () => {
    const wrapper = shallowMount(Bookmarks, {
      propsData: {
        bookmarks
      }
    });
    wrapper.findComponent(Bookmark).vm.$emit("tag-filtred");
    expect(wrapper.emitted("tag-filtred")).toBeTruthy();
  });
});
