import { shallowMount } from "@vue/test-utils";
import Bookmark from "@/components/Bookmark.vue";
import FilterStore from "@/stores/filter.store";

describe("Bookmark", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallowMount(Bookmark, {
      propsData: {
        title: "bookmark",
        description: "Awesome description",
        link: "https://example.com",
        tags: ["awesome", "tags"],
        date: new Date().toISOString()
      },
      data: () => ({
        store: FilterStore.state
      })
    });
  });

  it("should display bookmark informations", () => {
    expect(wrapper.html()).toContain(
      '<h3><a href="https://example.com" target="_blank">bookmark</a></h3>'
    );
    expect(wrapper.html()).toContain("<p>Awesome description</p>");
    expect(wrapper.html()).toContain(
      `<li><button class="">awesome</button></li>`
    );
    expect(wrapper.html()).toContain(`<li><button class="">tags</button></li>`);
  });

  it("should apply tag filter when user clic on tag", () => {
    const spyFilter = jest.spyOn(FilterStore, "setActiveTag");
    wrapper.find("button").trigger("click");
    expect(spyFilter).toBeCalledWith("awesome");
  });

  it("should remove tag filter when user clic on current tag filter", () => {
    const spyFilter = jest.spyOn(FilterStore, "setActiveTag");
    wrapper.find("button").trigger("click");
    expect(spyFilter).toBeCalledWith(null);
  });
});
