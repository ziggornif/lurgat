import { shallowMount } from "@vue/test-utils";
import Searchbar from "@/components/Searchbar.vue";
import FilterStore from "@/stores/filter.store";

describe("Searchbar", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallowMount(Searchbar, {
      propsData: {
        minChars: 3
      },
      data: () => ({
        store: FilterStore.state
      })
    });
  });

  it("should render search input", () => {
    expect(wrapper.html()).toContain(
      '<input name="search" placeholder="Chercher un lien..."><button>×</button>'
    );
  });

  it("should start filling input and do nothin (< 3 chars)", async () => {
    const input = wrapper.find("input");
    input.setValue("te");
    await input.trigger("keyup", {
      key: "e"
    });
    expect(wrapper.emitted("search-cancel")).toBeFalsy();
    expect(wrapper.emitted("search")).toBeFalsy();
  });

  it("should trigger search", async () => {
    const input = wrapper.find("input");
    input.setValue("test");
    await input.trigger("keyup", {
      key: "t"
    });
    expect(wrapper.emitted("search-cancel")).toBeFalsy();
    expect(wrapper.emitted("search")).toBeTruthy();
  });

  it("should trigger cancel search", async () => {
    const input = wrapper.find("input");
    input.setValue("test");
    await input.trigger("keyup", {
      key: "t"
    });
    input.setValue("te");
    await input.trigger("keyup", {
      key: "e"
    });
    expect(wrapper.emitted("search")).toBeTruthy();
    expect(wrapper.emitted("search-cancel")).toBeTruthy();
  });

  it("should erase search", async () => {
    const input = wrapper.find("input");
    input.setValue("test");
    await input.trigger("keyup", {
      key: "t"
    });
    input.setValue("");
    await input.trigger("keyup", {
      key: "e"
    });
    expect(wrapper.emitted("search")).toBeTruthy();
    expect(wrapper.emitted("search-cancel")).toBeTruthy();
  });

  it("should trigger cancel search", async () => {
    const input = wrapper.find("input");
    input.setValue("test");
    await input.trigger("keyup", {
      key: "t"
    });

    const button = wrapper.find("button");
    button.trigger("click");
    expect(wrapper.emitted("search-cancel")).toBeTruthy();
  });
});
