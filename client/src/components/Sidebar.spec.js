import { shallowMount } from "@vue/test-utils";
import flushPromises from "flush-promises";
import Sidebar from "@/components/Sidebar.vue";
import FilterStore from "@/stores/filter.store";
import axios from "axios";
jest.mock("axios");

describe("Sidebar", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it("should display sidebar", async () => {
    axios.mockImplementation(async () => {
      return {
        data: ["awesome", "tags"]
      };
    });

    const wrapper = shallowMount(Sidebar, {
      data: () => ({
        store: FilterStore.state
      })
    });

    await flushPromises();

    expect(wrapper.html()).toContain(
      '<li><button id="period-daily" class="">Aujourd\'hui</button></li>'
    );
    expect(wrapper.html()).toContain(
      '<li><button id="period-weekly" class="">Cette semaine</button></li>'
    );
    expect(wrapper.html()).toContain(
      '<li><button id="period-monthly" class="">Ce mois</button></li>'
    );
    expect(wrapper.html()).toContain(
      '<li><button id="tag-awesome" class="">awesome</button></li>'
    );
    expect(wrapper.html()).toContain(
      '<li><button id="tag-tags" class="">tags</button></li>'
    );
    expect(axios).toBeCalled();
  });

  it("should filter by date", async () => {
    axios.mockImplementation(async () => {
      return {
        data: ["awesome", "tags"]
      };
    });

    const wrapper = shallowMount(Sidebar, {
      data: () => ({
        store: FilterStore.state
      })
    });

    await flushPromises();

    const button = wrapper.find("#period-daily");
    button.trigger("click");
    expect(wrapper.emitted("tag-filtred")).toBeTruthy();
    expect(axios).toBeCalled();
  });

  it("should filter by tag", async () => {
    axios.mockImplementation(async () => {
      return {
        data: ["awesome", "tags"]
      };
    });

    const wrapper = shallowMount(Sidebar, {
      data: () => ({
        store: FilterStore.state
      })
    });

    await flushPromises();

    const button = wrapper.find("#tag-awesome");
    button.trigger("click");
    expect(wrapper.emitted("tag-filtred")).toBeTruthy();
    expect(FilterStore.state.activeTag).toBe("awesome");
    expect(axios).toBeCalled();
  });
});
