import axios from "axios";
import {
  getTags,
  getBookmarks,
  fullTextQuery,
  filter
} from "@/services/bookmark.service";
jest.mock("axios");

describe("Bookmark service", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it("should get tags from API", async () => {
    axios.mockImplementation(async () => {
      return {
        data: ["awesome", "tags"]
      };
    });

    const tags = await getTags();
    expect(tags).toEqual(["awesome", "tags"]);
  });

  it("should get bookmarks from API", async () => {
    const bookmarksMock = [
      {
        id: "1",
        title: "bookmark",
        description: "Awesome description",
        link: "https://example.com",
        tags: ["awesome", "tags"],
        date: new Date().toISOString()
      },
      {
        id: "2",
        title: "click here",
        description: "wow !",
        link: "https://my-link.com",
        tags: ["shitty", "post"],
        date: new Date().toISOString()
      }
    ];
    axios.mockImplementation(async () => {
      return {
        data: bookmarksMock
      };
    });

    const bookmarks = await getBookmarks();
    expect(bookmarks).toHaveLength(2);
    expect(bookmarks).toEqual(bookmarksMock);
  });

  it("should call bookmarks fulltext API", async () => {
    const bookmarksMock = [
      {
        id: "1",
        title: "bookmark",
        description: "Awesome description",
        link: "https://example.com",
        tags: ["awesome", "tags"],
        date: new Date().toISOString()
      }
    ];

    axios.mockImplementation(async () => {
      return {
        data: bookmarksMock
      };
    });

    const bookmarks = await fullTextQuery("book");
    expect(axios).toHaveBeenCalledWith({
      method: "GET",
      url: "/api/bookmarks/?search=book"
    });
    expect(bookmarks).toEqual(bookmarksMock);
  });

  it("should call bookmarks filter API", async () => {
    const bookmarksMock = [
      {
        id: "1",
        title: "bookmark",
        description: "Awesome description",
        link: "https://example.com",
        tags: ["awesome", "tags"],
        date: new Date().toISOString()
      }
    ];

    axios.mockImplementation(async () => {
      return {
        data: bookmarksMock
      };
    });

    const bookmarks = await filter("daily", "awesome");
    expect(axios).toHaveBeenCalledWith({
      method: "GET",
      url: "/api/bookmarks/",
      params: {
        since: "daily",
        tags: "awesome"
      }
    });
    expect(bookmarks).toEqual(bookmarksMock);
  });
});
