import axios from "axios";

async function getTags() {
  const { data } = await axios({
    method: "GET",
    url: "/api/tags/"
  });
  return data;
}

async function getBookmarks() {
  const { data } = await axios({
    method: "GET",
    url: "/api/bookmarks/"
  });
  return data;
}

async function fullTextQuery(text) {
  const { data } = await axios({
    method: "GET",
    url: `/api/bookmarks/?search=${text}`
  });
  return data;
}

async function filter(date, tag) {
  const { data } = await axios({
    method: "GET",
    url: "/api/bookmarks/",
    params: {
      since: date,
      tags: tag
    }
  });
  return data;
}

export { getTags, getBookmarks, fullTextQuery, filter };
