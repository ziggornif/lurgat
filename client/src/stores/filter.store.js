import { reactive } from "vue";

const store = {
  debug: true,

  state: reactive({
    activeTag: null,
    activeDate: null,
    searchQuery: null
  }),

  setActiveTag(newValue) {
    this.state.activeTag = newValue;
  },

  setActiveDate(newValue) {
    this.state.activeDate = newValue;
  },

  setSearchQuery(search) {
    this.state.searchQuery = search;
  },

  clearFilters() {
    this.state.activeDate = null;
    this.state.activeTag = null;
  },

  clearSearch() {
    this.state.searchQuery = null;
  }
};

export default store;
