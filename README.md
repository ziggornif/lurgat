# lurgat

<img src="./logo.svg" alt="logo" width="100">

Bookmark and tag your links.

## Requirements

Before running this project locally, you need these dependencies
- Go 1.15+
- Node 14+ and npm
- docker

## Building the server

```
make build
```

## Running the server locally

Run the following command :

```
make run
```

Open browser to http://localhost:8080 to start.

## Testing and linting

Run linting task :

```
make lint
```

Run tests suites :

```
make test
```
