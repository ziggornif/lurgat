package error

import (
	"fmt"
)

type LurgatError struct {
	Kind  string
	Error error
}

func (err LurgatError) String() string {
	return fmt.Sprintf("%s Error - %s", err.Kind, err.Error.Error())
}
