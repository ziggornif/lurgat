package bookmark

import (
	"encoding/json"
	"time"
)

type Bookmark struct {
	ID          string    `pg:",pk" json:"id"`
	Title       string    `pg:",unique" json:"title"`
	Link        string    `json:"link"`
	Description string    `json:"description"`
	Tags        []string  `json:"tags"`
	CreatedAt   time.Time `json:"createdAt"`
	UpdatedAt   time.Time `json:"updatedAt"`
}

// FullTextObject - ES query mapping object
type FullTextObject struct {
	Query string `json:"query"`
}

// MatchObject - ES query mapping object
type MatchObject struct {
	FullText *FullTextObject `json:"fullText,omitempty"`
	Tags     string          `json:"tags,omitempty"`
}

// CreatedRangeObject - ES query mapping object
type CreatedRangeObject struct {
	Gte string `json:"gte,omitempty"`
	Lte string `json:"lte,omitempty"`
}

// RangeObject - ES query mapping object
type RangeObject struct {
	CreatedAt *CreatedRangeObject `json:"createdAt,omitempty"`
}

// MustObject - ES query mapping object
type MustObject struct {
	Match *MatchObject `json:"match,omitempty"`
	Range *RangeObject `json:"range,omitempty"`
}

// BoolObject - ES query mapping object
type BoolObject struct {
	Must *[]MustObject `json:"must,omitempty"`
}

// QueryObject - ES query mapping object
type QueryObject struct {
	Bool *BoolObject `json:"bool,omitempty"`
}

// CreatedSortObject - ES query mapping object
type CreatedSortObject struct {
	Order string `json:"order"`
}

// SortObject - ES query mapping object
type SortObject struct {
	CreatedAt CreatedSortObject `json:"createdAt"`
}

// ESQuery - ES query mapping object
type ESQuery struct {
	Query *QueryObject `json:"query,omitempty"`
	Size  int          `json:"size"`
	Sort  []SortObject `json:"sort"`
}

// JSONString - Transform ES query to json string
func (e ESQuery) JSONString() string {
	byteArray, _ := json.Marshal(e)
	return string(byteArray)
}

// ESUpdate - ES query mapping object
type ESUpdate struct {
	Doc *Bookmark `json:"doc"`
}
