package bookmark

type Repository interface {
	CreateSchema() error
	Insert(bookmark *Bookmark) error
	Fetch(search string, since string, tags string) ([]Bookmark, error)
	FindByID(id string) (*Bookmark, error)
	Update(bookmark *Bookmark) error
	Delete(id string) error
	DeleteALl() error
}
