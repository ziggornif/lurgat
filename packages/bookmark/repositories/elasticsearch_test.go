package repos

import (
	"lurgat/infra"
	"lurgat/infra/storage"
	"lurgat/packages/bookmark"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestESInsert(t *testing.T) {
	logger := infra.GetLogger()
	config, _ := infra.LoadConfig("../../..", logger)
	es, _ := storage.ESConnect(config.GetConfig(), logger)
	es.DeleteIndex("bookmarks")

	repo, _ := NewElasticRepo(es, logger)
	err := repo.Insert(&bookmark.Bookmark{
		ID:          "1234",
		Title:       "Test",
		Description: "Test",
		Link:        "https://example.com",
		Tags:        []string{"awesome", "tags"},
	})
	assert.Nil(t, err)

	time.Sleep(1 * time.Second)

	created, _ := repo.Fetch("Test", "", "")
	assert.Equal(t, created[0].ID, "1234")
	assert.Equal(t, created[0].Title, "Test")
	assert.Equal(t, created[0].Description, "Test")
	assert.Equal(t, created[0].Link, "https://example.com")
	assert.EqualValues(t, created[0].Tags, []string{"awesome", "tags"})


	errDel := repo.Delete("1234")
	assert.Nil(t, errDel)

	time.Sleep(1 * time.Second)

	es.DeleteIndex("bookmarks")
}
