package repos

import (
	"lurgat/infra"
	"lurgat/infra/storage"
	"lurgat/packages/bookmark"
	"testing"

	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"github.com/stretchr/testify/assert"
)

func cleanInserted(db *pg.DB, id string) {
	_, _ = db.Model(&bookmark.Bookmark{}).Where("id = ?", id).Delete()
}

func TestPGNewRepoError(t *testing.T) {
	logger := infra.GetLogger()
	config, _ := infra.LoadConfig("../../..", logger)
	db, _ := storage.DBConnect(config.GetConfig(), logger)
	_ = db.Close()

	_, err := NewPostgresRepo(db, logger)
	assert.NotNil(t, err)
	assert.Equal(t, err.Error(), "pg: database is closed")
	storage.DBClose(logger)
}

func TestPGInsert(t *testing.T) {
	logger := infra.GetLogger()
	config, _ := infra.LoadConfig("../../..", logger)
	db, _ := storage.DBConnect(config.GetConfig(), logger)

	repo, _ := NewPostgresRepo(db, logger)

	err := repo.Insert(&bookmark.Bookmark{
		ID:          "1234",
		Title:       "Test",
		Description: "Test",
		Link:        "https://example.com",
		Tags:        []string{"awesome", "tags"},
	})
	assert.Nil(t, err)

	created, _ := repo.FindByID("1234")
	assert.Equal(t, created.ID, "1234")
	assert.Equal(t, created.Title, "Test")
	assert.Equal(t, created.Description, "Test")
	assert.Equal(t, created.Link, "https://example.com")
	assert.EqualValues(t, created.Tags, []string{"awesome", "tags"})

	cleanInserted(db, "1234")
}

func TestPGInsertError(t *testing.T) {
	logger := infra.GetLogger()
	config, _ := infra.LoadConfig("../../..", logger)
	db, _ := storage.DBConnect(config.GetConfig(), logger)

	repo, _ := NewPostgresRepo(db, logger)
	_ = db.Model((*bookmark.Bookmark)(nil)).DropTable(&orm.DropTableOptions{IfExists: true})

	err := repo.Insert(&bookmark.Bookmark{
		ID:          "1234",
		Title:       "Test",
		Description: "Test",
		Link:        "https://example.com",
		Tags:        []string{"awesome", "tags"},
	})

	assert.NotNil(t, err)
	assert.Equal(t, err.Error(), "ERROR #42P01 relation \"bookmarks\" does not exist")
}

func TestPGFetch(t *testing.T) {
	logger := infra.GetLogger()
	config, _ := infra.LoadConfig("../../..", logger)
	db, _ := storage.DBConnect(config.GetConfig(), logger)

	repo, _ := NewPostgresRepo(db, logger)

	err := repo.Insert(&bookmark.Bookmark{
		ID:          "1234",
		Title:       "Test",
		Description: "Test",
		Link:        "https://example.com",
		Tags:        []string{"awesome", "tags"},
	})
	assert.Nil(t, err)

	bookmarks, _ := repo.Fetch("", "", "")

	assert.Equal(t, len(bookmarks), 1)
	assert.Equal(t, bookmarks[0].ID, "1234")
	assert.Equal(t, bookmarks[0].Title, "Test")
	assert.Equal(t, bookmarks[0].Description, "Test")
	assert.Equal(t, bookmarks[0].Link, "https://example.com")
	assert.EqualValues(t, bookmarks[0].Tags, []string{"awesome", "tags"})

	cleanInserted(db, "1234")
}

func TestPGFindByIDError(t *testing.T) {
	logger := infra.GetLogger()
	config, _ := infra.LoadConfig("../../..", logger)
	db, _ := storage.DBConnect(config.GetConfig(), logger)

	repo, _ := NewPostgresRepo(db, logger)
	_ = db.Model((*bookmark.Bookmark)(nil)).DropTable(&orm.DropTableOptions{IfExists: true})

	_, err := repo.FindByID("1234")

	assert.NotNil(t, err)
	assert.Equal(t, err.Error(), "ERROR #42P01 relation \"bookmarks\" does not exist")
}

func TestPGUpdate(t *testing.T) {
	logger := infra.GetLogger()
	config, _ := infra.LoadConfig("../../..", logger)
	db, _ := storage.DBConnect(config.GetConfig(), logger)

	repo, _ := NewPostgresRepo(db, logger)

	_ = repo.Insert(&bookmark.Bookmark{
		ID:          "1234",
		Title:       "Test",
		Description: "Test",
		Link:        "https://example.com",
		Tags:        []string{"awesome", "tags"},
	})

	err := repo.Update(&bookmark.Bookmark{
		ID:          "1234",
		Title:       "TestUpdated",
		Description: "Test updated",
		Link:        "https://example.com/updated",
		Tags:        []string{"awesome", "tags", "new"},
	})
	assert.Nil(t, err)

	updated, _ := repo.FindByID("1234")
	assert.Equal(t, updated.ID, "1234")
	assert.Equal(t, updated.Title, "TestUpdated")
	assert.Equal(t, updated.Description, "Test updated")
	assert.Equal(t, updated.Link, "https://example.com/updated")
	assert.EqualValues(t, updated.Tags, []string{"awesome", "tags", "new"})

	cleanInserted(db, "1234")
}

func TestPGUpdateError(t *testing.T) {
	logger := infra.GetLogger()
	config, _ := infra.LoadConfig("../../..", logger)
	db, _ := storage.DBConnect(config.GetConfig(), logger)

	repo, _ := NewPostgresRepo(db, logger)
	_ = db.Model((*bookmark.Bookmark)(nil)).DropTable(&orm.DropTableOptions{IfExists: true})

	err := repo.Update(&bookmark.Bookmark{
		ID:          "1234",
		Title:       "TestUpdated",
		Description: "Test updated",
		Link:        "https://example.com/updated",
		Tags:        []string{"awesome", "tags", "new"},
	})

	assert.NotNil(t, err)
	assert.Equal(t, err.Error(), "ERROR #42P01 relation \"bookmarks\" does not exist")
}

func TestPGDelete(t *testing.T) {
	logger := infra.GetLogger()
	config, _ := infra.LoadConfig("../../..", logger)
	db, _ := storage.DBConnect(config.GetConfig(), logger)

	repo, _ := NewPostgresRepo(db, logger)

	_ = repo.Insert(&bookmark.Bookmark{
		ID:          "1234",
		Title:       "Test",
		Description: "Test",
		Link:        "https://example.com",
		Tags:        []string{"awesome", "tags"},
	})

	err := repo.Delete("1234")
	assert.Nil(t, err)

	res, _ := repo.FindByID("1234")
	assert.Empty(t, res)
}

func TestPGDeleteError(t *testing.T) {
	logger := infra.GetLogger()
	config, _ := infra.LoadConfig("../../..", logger)
	db, _ := storage.DBConnect(config.GetConfig(), logger)

	repo, _ := NewPostgresRepo(db, logger)
	_ = db.Model((*bookmark.Bookmark)(nil)).DropTable(&orm.DropTableOptions{IfExists: true})

	err := repo.Delete("1234")

	assert.NotNil(t, err)
	assert.Equal(t, err.Error(), "ERROR #42P01 relation \"bookmarks\" does not exist")
}

func TestPGDeleteAll(t *testing.T) {
	logger := infra.GetLogger()
	config, _ := infra.LoadConfig("../../..", logger)
	db, _ := storage.DBConnect(config.GetConfig(), logger)

	repo, _ := NewPostgresRepo(db, logger)
	_ = db.Model((*bookmark.Bookmark)(nil)).DropTable(&orm.DropTableOptions{IfExists: true})

	err := repo.DeleteALl()

	assert.NotNil(t, err)
	assert.Equal(t, err.Error(), "not implemented")
}
