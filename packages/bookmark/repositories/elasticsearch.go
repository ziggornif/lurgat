package repos

import (
	"errors"
	"io"
	"lurgat/infra/storage"
	"lurgat/packages/bookmark"
	"strings"
	"time"

	"github.com/elastic/go-elasticsearch/v7/esutil"
	"github.com/sirupsen/logrus"
	"github.com/tidwall/gjson"
)

type esRepository struct {
	elastic *storage.Elastic
	logger  *logrus.Logger
}

func parseESBookmark(esBookmark gjson.Result) bookmark.Bookmark {
	createdAt, _ := time.Parse(
		time.RFC3339,
		esBookmark.Get("createdAt").String())

	parsed := bookmark.Bookmark{
		ID:          esBookmark.Get("id").String(),
		Title:       esBookmark.Get("title").String(),
		Description: esBookmark.Get("description").String(),
		Link:        esBookmark.Get("link").String(),
		CreatedAt:   createdAt,
	}

	for _, tag := range esBookmark.Get("tags").Array() {
		parsed.Tags = append(parsed.Tags, tag.String())
	}

	return parsed
}

func prepareFetchQuery(search string, since string, tags string, logger *logrus.Logger) bookmark.ESQuery {
	// Skip function
	if len(search) == 0 && len(since) == 0 && len(tags) == 0 {
		logger.Debug("[BookmarkRepository] prepareFetchQuery - Empty query")
		return bookmark.ESQuery{
			Sort: []bookmark.SortObject{{
				CreatedAt: bookmark.CreatedSortObject{
					Order: "desc",
				},
			}},
			Size: 10000,
		}
	}

	query := bookmark.ESQuery{
		Query: &bookmark.QueryObject{
			Bool: &bookmark.BoolObject{
				Must: &[]bookmark.MustObject{},
			},
		},
		Sort: []bookmark.SortObject{{
			CreatedAt: bookmark.CreatedSortObject{
				Order: "desc",
			},
		}},
		Size: 10000,
	}

	if len(search) > 0 {
		*query.Query.Bool.Must = append(*query.Query.Bool.Must, bookmark.MustObject{
			Match: &bookmark.MatchObject{
				FullText: &bookmark.FullTextObject{
					Query: search,
				},
			}})
	}

	if len(tags) > 0 {
		*query.Query.Bool.Must = append(*query.Query.Bool.Must, bookmark.MustObject{
			Match: &bookmark.MatchObject{
				Tags: tags,
			},
		})
	}

	if len(since) > 0 {
		var period string
		switch since {
		case "weekly":
			period = "now-7d/d"
		case "monthly":
			period = "now-1M/M"
		default:
			period = "now-1d/d"
		}

		*query.Query.Bool.Must = append(*query.Query.Bool.Must, bookmark.MustObject{
			Range: &bookmark.RangeObject{
				CreatedAt: &bookmark.CreatedRangeObject{
					Gte: period,
					Lte: "now",
				},
			},
		})
	}

	logger.Debug("[BookmarkRepository] prepareFetchQuery - Query :", query.JSONString())
	return query
}

func NewElasticRepo(elastic *storage.Elastic, logger *logrus.Logger) (bookmark.Repository, error) {
	esRepo := &esRepository{
		elastic: elastic,
		logger:  logger,
	}

	err := esRepo.elastic.CreateIndex("bookmarks", bookmark.GetESMapping())
	if err != nil {
		return nil, err
	}

	return esRepo, err
}

func (r *esRepository) CreateSchema() error {
	return r.elastic.CreateIndex("bookmarks", bookmark.GetESMapping())
}

func (r *esRepository) Insert(entity *bookmark.Bookmark) error {
	_, err := r.elastic.Client.Index("bookmarks", esutil.NewJSONReader(&entity), r.elastic.Client.Index.WithDocumentID(entity.ID))
	if err != nil {
		r.logger.Errorf("[BookmarkRepository] Insert - Error while indexing bookmark, Reason: %v", err)
		return err
	}
	return nil
}

func (r *esRepository) Fetch(search string, since string, tags string) ([]bookmark.Bookmark, error) {
	query := prepareFetchQuery(search, since, tags, r.logger)

	resBytes, err := r.elastic.Search("bookmarks", query.JSONString())
	if err != nil {
		r.logger.Errorf("[BookmarkRepository] Search - Error while searching bookmarks, Reason: %v", err)
		return nil, err
	}

	results := gjson.GetBytes(resBytes.Bytes(), "hits.hits").Array()

	var bookmarks []bookmark.Bookmark

	for _, result := range results {
		source := result.Get("_source")
		parsed := parseESBookmark(source)
		bookmarks = append(bookmarks, parsed)
	}

	return bookmarks, nil
}

func (r *esRepository) FindByID(string) (*bookmark.Bookmark, error) {
	return nil, errors.New("not implemented")
}

func (r *esRepository) Update(entity *bookmark.Bookmark) error {
	esUpdateQuery := bookmark.ESUpdate{
		Doc: entity,
	}
	_, err := r.elastic.Client.Update("bookmarks", entity.ID, esutil.NewJSONReader(&esUpdateQuery))
	if err != nil {
		r.logger.Errorf("[BookmarkRepository] Update - Error while indexing bookmark, Reason: %v", err)
		return err
	}

	return nil
}

func (r *esRepository) Delete(id string) error {
	_, err := r.elastic.Client.Delete("bookmarks", id)
	if err != nil {
		r.logger.Errorf("[BookmarkRepository] Delete - Error while removing bookmark from ES, Reason: %v", err)
		return err
	}

	return nil
}

func (r *esRepository) DeleteALl() error {
	res, err := r.elastic.Client.DeleteByQuery(
		[]string{"bookmarks"},
		strings.NewReader(`{
			"query": {
				"match_all": {}
			}
		}`),
	)
	if err != nil {
		r.logger.Errorf("[BookmarkRepository] Delete - Error while removing bookmark from ES, Reason: %v", err)
		return err
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			r.logger.Errorf("[BookmarkRepository] Delete - Error while removing bookmark from ES, Reason: %v", err)
		}
	}(res.Body)
	return nil
}
