package repos

import (
	"errors"

	"lurgat/packages/bookmark"

	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"github.com/sirupsen/logrus"
)

type repository struct {
	db     *pg.DB
	logger *logrus.Logger
}

func NewPostgresRepo(db *pg.DB, logger *logrus.Logger) (bookmark.Repository, error) {
	repo := &repository{
		db:     db,
		logger: logger,
	}

	err := repo.CreateSchema()
	if err != nil {
		return nil, err
	}

	return repo, nil
}

func (r *repository) CreateSchema() error {
	opts := &orm.CreateTableOptions{
		IfNotExists: true,
	}

	createErr := r.db.Model((*bookmark.Bookmark)(nil)).CreateTable(opts)
	if createErr != nil {
		r.logger.Errorf("[BookmarkRepository] CreateSchema - Error while creating table bookmarks, Reason: %v", createErr)
		return createErr
	}

	r.logger.Info("[BookmarkRepository] CreateSchema - Bookmark table created")

	return nil
}

func (r *repository) Insert(bookmark *bookmark.Bookmark) error {
	_, insertErr := r.db.Model(bookmark).Insert()
	if insertErr != nil {
		r.logger.Errorf("[BookmarkRepository] Insert - Error while inserting bookmark, Reason: %v", insertErr)
		return insertErr
	}
	r.logger.Info("[BookmarkRepository] Insert - Bookmark item created")
	return nil
}

func (r *repository) Fetch(string, string, string) ([]bookmark.Bookmark, error) {
	var bookmarks []bookmark.Bookmark
	getError := r.db.Model(&bookmarks).Select()
	if getError != nil {
		r.logger.Errorf("[BookmarkRepository] List - Error while getting bookmarks, Reason: %v", getError)
		return nil, getError
	}
	return bookmarks, nil

}

func (r *repository) FindByID(id string) (*bookmark.Bookmark, error) {
	found := &bookmark.Bookmark{}
	err := r.db.Model(found).Where("id = ?", id).Select()
	if err != nil {
		r.logger.Errorf("[BookmarkRepository] FindById - Error while getting bookmark, Reason: %v", err)
		return nil, err
	}
	return found, nil
}

func (r *repository) Update(bookmark *bookmark.Bookmark) error {
	_, err := r.db.Model(bookmark).WherePK().Update()
	if err != nil {
		r.logger.Errorf("[BookmarkRepository] Update - Error while inserting bookmark, Reason: %v", err)
		return err
	}
	r.logger.Info("[BookmarkRepository] Update - Bookmark item updated")
	return nil
}

func (r *repository) Delete(id string) error {
	_, err := r.db.Model(&bookmark.Bookmark{ID: id}).WherePK().Delete()
	if err != nil {
		r.logger.Errorf("[BookmarkRepository] Delete - Error while removing bookmark, Reason: %v", err)
		return err
	}
	r.logger.Info("[BookmarkRepository] Delete - Bookmark item removed")
	return nil
}

func (r *repository) DeleteALl() error {
	return errors.New("not implemented")
}
