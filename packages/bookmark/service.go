package bookmark

import (
	"errors"
	"sync"
	"time"

	bookmarksdto "lurgat/handlers/rest/models"
	"lurgat/packages/error"

	"github.com/go-playground/validator/v10"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
)

type Service interface {
	CreateBookmark(bookmarkDTO bookmarksdto.PostRequest) (*Bookmark, *error.LurgatError)
	ListBookmarks(fetchQuery bookmarksdto.FetchQuery) ([]Bookmark, *error.LurgatError)
	GetBookmark(id string) (*Bookmark, *error.LurgatError)
	UpdateBookmark(id string, bookmarkDTO bookmarksdto.UpdateRequest) (*Bookmark, *error.LurgatError)
	DeleteBookmark(id string) *error.LurgatError
	ResyncBookmarks() *error.LurgatError
}

type service struct {
	pgRepo   Repository
	esRepo   Repository
	logger   *logrus.Logger
	validate *validator.Validate
}

func NewService(pgRepo Repository, esRepo Repository, logger *logrus.Logger) Service {
	return &service{
		pgRepo:   pgRepo,
		esRepo:   esRepo,
		logger:   logger,
		validate: validator.New(),
	}
}

func (s *service) CreateBookmark(bookmarkDTO bookmarksdto.PostRequest) (*Bookmark, *error.LurgatError) {
	err := s.validate.Struct(bookmarkDTO)
	if err != nil {
		s.logger.Errorf("[BookmarkService] CreateBookmark - Bookmark must contains a title and a link.")
		s.logger.Debugf("... context : %s", err)
		return nil, &error.LurgatError{Kind: "Validation", Error: errors.New("bookmark must contains a title and a link")}
	}

	id, _ := uuid.NewV4()
	bookmark := Bookmark{
		ID:          id.String(),
		Title:       bookmarkDTO.Title,
		Description: bookmarkDTO.Description,
		Tags:        bookmarkDTO.Tags,
		Link:        bookmarkDTO.Link,
		CreatedAt:   time.Now(),
	}

	err = s.pgRepo.Insert(&bookmark)

	if err != nil {
		s.logger.Errorf("[BookmarkService] CreateBookmark - Error while creating bookmark")
		s.logger.Debugf("... context : %s", err)
		return nil, &error.LurgatError{Error: errors.New("creation error")}
	}

	err = s.esRepo.Insert(&bookmark)
	if err != nil {
		s.logger.Errorf("[BookmarkService] CreateBookmark - Error while indexing bookmark")
		s.logger.Debugf("... context : %s", err)
		return nil, &error.LurgatError{Error: errors.New("indexation error")}
	}

	s.logger.Info("[BookmarkService] CreateBookmark - Bookmark item created")
	return &bookmark, nil
}

func (s *service) ListBookmarks(fetchQuery bookmarksdto.FetchQuery) ([]Bookmark, *error.LurgatError) {
	err := s.validate.Struct(fetchQuery)
	if err != nil {
		s.logger.Errorf("[BookmarkService] ListBookmarks - Validation error.")
		s.logger.Debugf("... context : %s", err)
		return nil, &error.LurgatError{Kind: "Validation", Error: errors.New("bookmark must contains a title and a link")}
	}

	bookmarks, err := s.esRepo.Fetch(fetchQuery.Search, fetchQuery.Since, fetchQuery.Tag)
	if err != nil {
		s.logger.Errorf("[BookmarkService] ListBookmarks - Error while getting bookmarks")
		s.logger.Debugf("... context : %s", err)
		return nil, &error.LurgatError{Error: err}
	}

	return bookmarks, nil
}

func (s *service) GetBookmark(id string) (*Bookmark, *error.LurgatError) {
	bookmark, err := s.pgRepo.FindByID(id)
	if err != nil {
		s.logger.Errorf("[BookmarkService] GetBookmark - Error while getting bookmark %s", id)
		s.logger.Debugf("... context : %s", err)
		return nil, &error.LurgatError{Error: err}
	}

	return bookmark, nil
}

func (s *service) UpdateBookmark(id string, bookmarkDTO bookmarksdto.UpdateRequest) (*Bookmark, *error.LurgatError) {
	err := s.validate.Struct(bookmarkDTO)
	if err != nil {
		s.logger.Errorf("[BookmarkService] UpdateBookMark - Validation error.")
		s.logger.Debugf("... context : %s", err)
		return nil, &error.LurgatError{Kind: "Validation", Error: errors.New("bookmark must contains a title and a link")}
	}

	bookmark, err := s.pgRepo.FindByID(id)
	if err != nil {
		s.logger.Errorf("[BookmarkService] UpdateBookMark - Bookmark not found.")
		s.logger.Debugf("... context : %s", err)
		return nil, &error.LurgatError{Error: err}
	}

	bookmark.Title = bookmarkDTO.Title
	bookmark.Description = bookmarkDTO.Description
	bookmark.Link = bookmarkDTO.Link
	bookmark.Tags = bookmarkDTO.Tags
	bookmark.UpdatedAt = time.Now()

	err = s.pgRepo.Update(bookmark)
	if err != nil {
		s.logger.Errorf("[BookmarkService] UpdateBookmark - Error while updating bookmark")
		s.logger.Debugf("... context : %s", err)
		return nil, &error.LurgatError{Error: err}
	}

	err = s.esRepo.Update(bookmark)
	if err != nil {
		s.logger.Errorf("[BookmarkService] UpdateBookmark - Error while updating bookmark")
		s.logger.Debugf("... context : %s", err)
		return nil, &error.LurgatError{Error: err}
	}

	s.logger.Info("[BookmarkRepository] Update - Bookmark item updated")
	return bookmark, nil
}

func (s *service) DeleteBookmark(id string) *error.LurgatError {
	err := s.pgRepo.Delete(id)
	if err != nil {
		s.logger.Errorf("[BookmarkService] DeleteBookmark - Error while removing bookmark %s", id)
		s.logger.Debugf("... context : %s", err)
		return &error.LurgatError{Error: err}
	}

	err = s.esRepo.Delete(id)
	if err != nil {
		s.logger.Errorf("[BookmarkService] DeleteBookmark - Error while removing bookmark %s", id)
		s.logger.Debugf("... context : %s", err)
		return &error.LurgatError{Error: err}
	}

	s.logger.Info("[BookmarkRepository] Delete - Bookmark item removed")
	return nil
}

func (s *service) ResyncBookmarks() *error.LurgatError {
	err := s.esRepo.DeleteALl()
	if err != nil {
		s.logger.Errorf("[BookmarkService] ResyncBookmarks - Error while resyncing bookmarks")
		s.logger.Debugf("... context : %s", err)
		return &error.LurgatError{Error: err}
	}

	dbBookmarks, err := s.pgRepo.Fetch("", "", "")
	if err != nil {
		s.logger.Errorf("[BookmarkService] ResyncBookmarks - Error while resyncing bookmarks")
		s.logger.Debugf("... context : %s", err)
		return &error.LurgatError{Error: err}
	}

	bookmarksLength := len(dbBookmarks)
	if bookmarksLength == 0 {
		return nil
	}

	var wg sync.WaitGroup
	wg.Add(bookmarksLength)

	for i := 0; i < bookmarksLength; i++ {
		go func(i int) {
			defer wg.Done()
			bookmark := dbBookmarks[i]
			s.logger.Debugf("[BookmarkService] ResyncBookmarks - resync bookmark %s", bookmark.ID)
			err := s.esRepo.Insert(&bookmark)
			if err != nil {
				s.logger.Errorf("[BookmarkService] ResyncBookmarks - Error while indexing bookmark %s", bookmark.ID)
				s.logger.Debugf("... context : %s", err)
			}
		}(i)
	}

	wg.Wait()
	return nil
}
