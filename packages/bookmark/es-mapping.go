package bookmark

var bookmarkMapping = `
{
  "settings": {
    "index": {
      "number_of_replicas": 0,
      "max_ngram_diff": 5,
      "max_result_window": 1000000
    },
    "analysis": {
      "filter": {
        "edge_1_10": {
          "type": "edge_ngram",
          "min_gram": 3,
          "max_gram": 10,
          "token_chars": [
            "letter",
            "digit"
          ]
        },
        "french_stemmer": {
          "type": "stemmer",
          "language": "light_french"
        }
      },
      "char_filter": {
        "replace_special_characters": {
          "type": "mapping",
          "mappings": [
            "& => et"
          ]
        }
      },
      "analyzer": {
        "stemmer_analyzer": {
          "tokenizer": "icu_tokenizer",
          "filter": [
            "word_delimiter_graph",
            "icu_folding",
            "french_stemmer"
          ],
          "char_filter": [
            "replace_special_characters"
          ]
        },
        "edge_analyzer": {
          "tokenizer": "icu_tokenizer",
          "filter": [
            "word_delimiter_graph",
            "icu_folding",
            "french_stemmer",
            "edge_1_10"
          ],
          "char_filter": [
            "replace_special_characters"
          ]
        },
        "search_analyzer": {
          "tokenizer": "icu_tokenizer",
          "filter": ["word_delimiter_graph", "icu_folding", "french_stemmer"],
          "char_filter": ["replace_special_characters"]
        }
      }
    }
  },
  "mappings": {
    "dynamic": false,
    "_meta": {
      "mappingVersion": 2
    },
    "properties": {
      "fullText": {
        "type": "text",
        "analyzer": "edge_analyzer",
        "search_analyzer": "search_analyzer"
      },
      "id": {
        "type": "keyword"
      },
      "title": {
        "type": "text",
        "copy_to": "fullText"
      },
      "description": {
        "type": "text",
        "copy_to": "fullText"
      },
      "link": {
        "type": "keyword"
      },
      "tags": {
        "type": "keyword"
      },
      "createdAt": {
        "type": "date"
      }
    }
  }
}`

// GetESMapping - Return bookmark elasticsearch mapping
func GetESMapping() string {
	return bookmarkMapping
}
