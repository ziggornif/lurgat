package tag

import (
	"errors"
	"lurgat/infra"
	"testing"

	"github.com/stretchr/testify/assert"
)

type repoMock struct{}

type repoMockErr struct{}

func (r *repoMock) Fetch() ([]string, error) {
	return []string{"awesome", "tags"}, nil
}

func (r *repoMockErr) Fetch() ([]string, error) {
	return nil, errors.New("something wrong")
}

func TestListTags(t *testing.T) {
	service := NewService(&repoMock{}, infra.GetLogger())
	tags, err := service.ListTags()
	assert.Equal(t, tags, []string{"awesome", "tags"})
	assert.Nil(t, err)
}

func TestListTagsError(t *testing.T) {
	service := NewService(&repoMockErr{}, infra.GetLogger())
	tags, err := service.ListTags()
	assert.Nil(t, tags)
	assert.Equal(t, err.Error.Error(), "something wrong")
}
