package tag

import (
	"lurgat/infra"
	"lurgat/infra/storage"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

var mapping = `{
  "mappings": {
    "dynamic": false,
    "_meta": {
      "mappingVersion": 2
    },
    "properties": {
      "id": {
        "type": "keyword"
      },
      "tags": {
        "type": "keyword"
      }
    }
  }
}`

func TestFetch(t *testing.T) {
	logger := infra.GetLogger()
	config, _ := infra.LoadConfig("../..", logger)
	elastic, _ := storage.ESConnect(config.GetConfig(), logger)
	err := elastic.CreateIndex("bookmarks", mapping)
	assert.Nil(t, err)

	_, indexErr := elastic.Client.Index("bookmarks", strings.NewReader(`{"id": "1234", "tags": ["awesome", "tags"]}`))
	assert.Nil(t, indexErr)

	time.Sleep(1 * time.Second)

	repo := NewRepo(elastic, infra.GetLogger())
	tags, err := repo.Fetch()
	assert.EqualValues(t, tags, []string{"awesome", "tags"})
	assert.Nil(t, err)
	elastic.DeleteIndex("bookmarks")
}

func TestFetchError(t *testing.T) {
	logger := infra.GetLogger()
	config, _ := infra.LoadConfig("../..", logger)
	elastic, _ := storage.ESConnect(config.GetConfig(), logger)

	repo := NewRepo(elastic, infra.GetLogger())
	tags, err := repo.Fetch()
	assert.Nil(t, tags)
	assert.NotNil(t, err)
	assert.Equal(t, err.Error(), "index not found")
}
