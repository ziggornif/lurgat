package tag

import (
	"lurgat/packages/error"

	"github.com/sirupsen/logrus"
)

type Service interface {
	ListTags() ([]string, *error.LurgatError)
}

type service struct {
	repo   Repository
	logger *logrus.Logger
}

func NewService(repo Repository, logger *logrus.Logger) Service {
	return &service{
		repo:   repo,
		logger: logger,
	}
}

func (s *service) ListTags() ([]string, *error.LurgatError) {
	tags, err := s.repo.Fetch()
	if err != nil {
		s.logger.Errorf("[TagService] ListTags - Error while getting tags")
		s.logger.Debugf("... context : %s", err)
		return nil, &error.LurgatError{Error: err}
	}

	return tags, nil
}
