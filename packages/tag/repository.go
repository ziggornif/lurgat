package tag

import (
	"lurgat/infra/storage"

	"github.com/sirupsen/logrus"
	"github.com/tidwall/gjson"
)

type Repository interface {
	Fetch() ([]string, error)
}

type repository struct {
	elastic *storage.Elastic
	logger  *logrus.Logger
}

func NewRepo(elastic *storage.Elastic, logger *logrus.Logger) Repository {
	return &repository{
		elastic: elastic,
		logger:  logger,
	}
}

func (r *repository) Fetch() ([]string, error) {
	query := TagsAggregateQuery{
		Size: 0,
		Aggs: AggregateObject{
			Tags: TagsObject{
				Terms: TermsObject{
					Field: "tags",
					Size:  10000,
				},
			},
		},
	}

	resBytes, err := r.elastic.Search("bookmarks", query.JsonString())
	if err != nil {
		r.logger.Errorf("[TagRepository] Search - Error while getting tags, Reason: %v", err)
		return nil, err
	}

	buckets := gjson.GetBytes(resBytes.Bytes(), "aggregations.tags.buckets").Array()
	var tags []string
	for _, bucket := range buckets {
		tags = append(tags, bucket.Get("key").String())
	}
	return tags, nil
}
