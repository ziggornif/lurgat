package tag

import "encoding/json"

type TermsObject struct {
	Field string `json:"field"`
	Size  int    `json:"size"`
}
type TagsObject struct {
	Terms TermsObject `json:"terms"`
}
type AggregateObject struct {
	Tags TagsObject `json:"tags"`
}

type TagsAggregateQuery struct {
	Size int             `json:"size"`
	Aggs AggregateObject `json:"aggs"`
}

func (t TagsAggregateQuery) JsonString() string {
	byteArray, _ := json.Marshal(t)
	return string(byteArray)
}
