FROM node:14 as ui
WORKDIR /ui
COPY client .
RUN npm ci && npm run build

FROM golang:1-alpine as builder
RUN apk update && apk add upx gcc make g++

WORKDIR /build
ADD . .
RUN go build -ldflags "-w -s" -o lurgat main.go

FROM alpine
COPY --from=builder /build/lurgat /bin/lurgat
RUN chmod +x /bin/lurgat

# Add static content
COPY --from=builder /build/doc ./doc
COPY --from=ui /ui/dist ./static

ENV GIN_MODE=release

ENTRYPOINT ["/bin/lurgat"]
