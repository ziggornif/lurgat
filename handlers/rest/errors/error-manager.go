package error

import (
	"net/http"

	"lurgat/packages/error"

	"github.com/gin-gonic/gin"
)

// SendHTTPError Manage http response error
func SendHTTPError(c *gin.Context, err *error.LurgatError) {
	var status int

	switch err.Kind {
	case "Validation":
		status = http.StatusBadRequest
	case "NotFound":
		status = http.StatusNotFound
	default:
		status = http.StatusInternalServerError
	}
	c.JSON(status, gin.H{"error": err.String()})
}
