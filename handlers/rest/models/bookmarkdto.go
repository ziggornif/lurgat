package models

type PostRequest struct {
	Title       string   `json:"title" validate:"required"`
	Description string   `json:"description"`
	Tags        []string `json:"tags"`
	Link        string   `json:"link" validate:"required"`
}

type UpdateRequest struct {
	Title       string   `json:"title" validate:"required"`
	Description string   `json:"description"`
	Tags        []string `json:"tags"`
	Link        string   `json:"link" validate:"required"`
}

type FetchQuery struct {
	Search string `json:"search"`
	Since  string `json:"since"`
	Tag    string `json:"tag"`
}
