package rest

import (
	error "lurgat/handlers/rest/errors"
	"lurgat/handlers/rest/models"
	"lurgat/packages/bookmark"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/sirupsen/logrus"
)

type BookmarkController interface {
	postBookmark(c *gin.Context)
	getBookmarks(c *gin.Context)
	getBookmark(c *gin.Context)
	updateBookmark(c *gin.Context)
	deleteBookmark(c *gin.Context)
	resyncBookmark(c *gin.Context)
}

type bcontroller struct {
	service bookmark.Service
	logger  *logrus.Logger
}

func (ctrl *bcontroller) postBookmark(c *gin.Context) {
	query := models.PostRequest{}
	if err := c.ShouldBindBodyWith(&query, binding.JSON); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	entity, err := ctrl.service.CreateBookmark(query)
	if err != nil {
		error.SendHTTPError(c, err)
		return
	}

	c.JSON(http.StatusOK, entity)
}

func (ctrl *bcontroller) getBookmarks(c *gin.Context) {
	tags, _ := c.GetQuery("tags")
	search, _ := c.GetQuery("search")
	since, _ := c.GetQuery("since")
	query := models.FetchQuery{
		Tag:    tags,
		Search: search,
		Since:  since,
	}

	bookmarks, err := ctrl.service.ListBookmarks(query)
	if err != nil {
		error.SendHTTPError(c, err)
		return
	}

	c.JSON(http.StatusOK, bookmarks)
}

func (ctrl *bcontroller) getBookmark(c *gin.Context) {
	bookmarkID := c.Param("id")
	entity, err := ctrl.service.GetBookmark(bookmarkID)
	if err != nil {
		error.SendHTTPError(c, err)
		return
	}

	c.JSON(http.StatusOK, entity)
}

func (ctrl *bcontroller) updateBookmark(c *gin.Context) {
	bookmarkID := c.Param("id")
	query := models.UpdateRequest{}
	if err := c.ShouldBindBodyWith(&query, binding.JSON); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	updatedBookmark, err := ctrl.service.UpdateBookmark(bookmarkID, query)

	if err != nil {
		error.SendHTTPError(c, err)
		return
	}
	c.JSON(http.StatusOK, updatedBookmark)
}

func (ctrl *bcontroller) deleteBookmark(c *gin.Context) {
	bookmarkID := c.Param("id")

	err := ctrl.service.DeleteBookmark(bookmarkID)
	if err != nil {
		error.SendHTTPError(c, err)
		return
	}
	c.Status(http.StatusNoContent)
}

func (ctrl *bcontroller) resyncBookmarks(c *gin.Context) {
	err := ctrl.service.ResyncBookmarks()
	if err != nil {
		error.SendHTTPError(c, err)
		return
	}
	c.Status(http.StatusNoContent)
}

func MakeBookmarkController(route *gin.RouterGroup, service bookmark.Service, logger *logrus.Logger) {
	controller := bcontroller{
		service: service,
		logger:  logger,
	}

	bookmarksRouter := route.Group("/bookmarks")
	{
		bookmarksRouter.POST("/", controller.postBookmark)
		bookmarksRouter.GET("/", controller.getBookmarks)
		bookmarksRouter.GET("/:id", controller.getBookmark)
		bookmarksRouter.PUT("/:id", controller.updateBookmark)
		bookmarksRouter.DELETE("/:id", controller.deleteBookmark)
		bookmarksRouter.POST("/resync", controller.resyncBookmarks)
	}
}
