package rest

import (
	error "lurgat/handlers/rest/errors"
	"lurgat/packages/tag"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

type TagController interface {
	getTags(c *gin.Context)
}

type tcontroller struct {
	service tag.Service
	logger  *logrus.Logger
}

func (ctrl *tcontroller) getTags(c *gin.Context) {
	tags, err := ctrl.service.ListTags()
	if err != nil {
		error.SendHTTPError(c, err)
		return
	}

	c.JSON(http.StatusOK, tags)
}

func MakeTagsController(route *gin.RouterGroup, service tag.Service, logger *logrus.Logger) {
	controller := tcontroller{
		service: service,
		logger:  logger,
	}

	tagsRouter := route.Group("/tags")
	{
		tagsRouter.GET("/", controller.getTags)
	}
}
