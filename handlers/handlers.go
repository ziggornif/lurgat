package handlers

import (
	"lurgat/handlers/rest"
	"lurgat/infra/storage"
	"lurgat/packages/bookmark"
	repos "lurgat/packages/bookmark/repositories"
	"lurgat/packages/tag"

	"github.com/gin-gonic/gin"
	"github.com/go-pg/pg/v10"
	"github.com/sirupsen/logrus"
)

func MakeRestHandler(route *gin.RouterGroup, db *pg.DB, elastic *storage.Elastic, logger *logrus.Logger) error {
	pgRepo, pgErr := repos.NewPostgresRepo(db, logger)
	if pgErr != nil {
		return pgErr
	}

	esRepo, esErr := repos.NewElasticRepo(elastic, logger)
	if esErr != nil {
		return esErr
	}

	service := bookmark.NewService(pgRepo, esRepo, logger)
	rest.MakeBookmarkController(route, service, logger)

	repo := tag.NewRepo(elastic, logger)
	tagService := tag.NewService(repo, logger)
	rest.MakeTagsController(route, tagService, logger)

	return nil
}
