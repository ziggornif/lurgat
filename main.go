package main

import (
	"log"
	"lurgat/handlers"
	"lurgat/infra"
	"lurgat/infra/monitoring"
	"lurgat/infra/storage"
	"net/http"
	"os"

	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

type app struct {
	logger *logrus.Logger
}

func NewApp(logger *logrus.Logger) *app{
	return &app{logger: logger}
}

func (a *app) run() {
	config, err := infra.LoadConfig(".", a.logger)
	if err != nil {
		log.Fatal("Unable to read the config file: ", err)
		return
	}

	config.ValidateConfig()

	db, errDB := storage.DBConnect(config.GetConfig(), a.logger)
	elastic, errES := storage.ESConnect(config.GetConfig(), a.logger)

	if errDB != nil || errES != nil {
		a.exit()
	}

	router := gin.Default()
	router.Use(cors.Default())
	router.Use(static.Serve("/", static.LocalFile("./static", false)))
	router.Use(static.Serve("/assets", static.LocalFile("./doc", false)))

	router.LoadHTMLFiles("./doc/swagger.html")
	router.GET("/docs", func(c *gin.Context) {
		c.HTML(http.StatusOK, "swagger.html", nil)
	})

	apiRouter := router.Group("/api")
	err = handlers.MakeRestHandler(apiRouter, db, elastic, a.logger)
	if err != nil {
		a.exit()
	}

	monitoring.PrometheusHandler(router)
	monitoring.HealthcheckHandler(router, db, elastic)
	monitoring.HeartbeatHandler(router)

	err = router.Run()
	if err != nil {
		a.exit()
	}
}

func (a *app) exit() {
	a.logger.Error("Could not start app ...") //TODO: improve this log message
	os.Exit(1)
}

func (a *app) shutdown() {
	storage.DBClose(a.logger)
	storage.ESClose()
}

func main() {
	logger := infra.GetLogger()

	app := NewApp(logger)
	app.run()

	defer app.shutdown()
}
